package wantsome.training.questions;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.targets.TheTarget;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import wantsome.training.ui.SearchResultsPage;

import java.util.List;

public class TheSearchResults {
    public static Question<List<String>> list() {
        return TheTarget.textValuesOf(SearchResultsPage.RESULTS);
    }
}
