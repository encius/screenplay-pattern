package wantsome.training.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SearchResultsPage {
    public static Target RESULTS = Target.the("list of results").
            located(By.cssSelector(".entry-title>a"));
}
