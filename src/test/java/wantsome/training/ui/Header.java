package wantsome.training.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Header extends PageObject {

    public static final Target SEARCH_ICON = Target.the("search icon").
            located(By.cssSelector(".search-icon"));
    public static final Target SEARCH_FIELD = Target.the("search field").
            locatedBy(".search-field");
}
