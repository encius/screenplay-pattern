package wantsome.training.ui;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;

@DefaultUrl("http://practica.wantsome.ro/shop")
public class ShopHomePage extends PageObject {

    public ShopHomePage(WebDriver driver) {
        super(driver);
    }
}
