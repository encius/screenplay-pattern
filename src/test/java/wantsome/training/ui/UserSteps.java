package wantsome.training.ui;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;

public class UserSteps extends ScenarioSteps {

    ShopHomePage shopHomePage;

    @Step
    public void navigatesToShopHomePage() {
        shopHomePage.open();
    }

    public void shouldSeeThatTheTitleContains(String pageTitle) {
        Assert.assertThat(shopHomePage.getTitle(),
                CoreMatchers.containsString(pageTitle));
    }
}
