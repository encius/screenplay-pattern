package wantsome.training.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.Keys;
import wantsome.training.ui.Header;

public class Search implements Task {
    private String product;

    public Search(String product) {
        this.product = product;
    }

    public static Performable forProduct(String product) {
        return Instrumented.instanceOf(Search.class).
                withProperties(product);
    }

    @Step("{0} searches for product #product")
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(Header.SEARCH_ICON),
                Enter.theValue(product).into(Header.SEARCH_FIELD).
                        thenHit(Keys.ENTER)
        );
    }
}
