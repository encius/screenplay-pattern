package wantsome.training.features.exercises;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import wantsome.training.ui.UserSteps;

@RunWith(SerenityRunner.class)
public class WhenBuyingAProductAsCustomer {

    @Managed
    WebDriver browser;

    Actor automator = Actor.named("The user");

    @Before
    public void setup() {
        automator.can(BrowseTheWeb.with(browser));
        automator.has(Open.url("http://practica.wantsome.ro/shop"));
    }

    @Test
    public void shouldSeeTheOrderInMyAccount() {
        // 1. Login with customer
        // 2. Navigate to home
        // 3. Click add on first(or X) product from men/women collection
        // 4. Go to cart
        // 5. Click on checkout
        // 6. Place order
        // 7. Verify order details
    }
}
