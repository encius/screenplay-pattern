package wantsome.training.features.examples;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.questions.page.TheWebPage;
import net.serenitybdd.screenplay.questions.targets.TheTarget;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import wantsome.training.questions.TheSearchResults;
import wantsome.training.tasks.Search;
import wantsome.training.ui.ShopHomePage;
import wantsome.training.ui.UserSteps;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.contains;

@RunWith(SerenityRunner.class)
public class WhenSearchingForAProduct {


    @Managed
    WebDriver browser;

    @Steps
    UserSteps user;

    Actor automator = Actor.named("Enciu");

//    @Before
//    public void setup() {
//        automator.can(BrowseTheWeb.with(browser));
//    }
    @Pending
    @Test
    public void theShopTitleShouldBeVisible() {
        browser.get("http://practica.wantsome.ro/shop");
        Assert.assertThat(browser.getTitle(),
                containsString("Wantsome Shop"));
    }

    @Pending
    @Test
    public void theShopTitleShouldBeVisibleWithPO() {
        ShopHomePage shopHomePage = new ShopHomePage(browser);
        shopHomePage.open();
    }

    @Pending
    @Test
    public void theShopTitleShouldBeVisibleWithSteps() {
        user.navigatesToShopHomePage();
        user.shouldSeeThatTheTitleContains("Wantsome Shop");
    }

    @Pending
    @Test
    public void theShopTitleShouldBeVisibleActor() {
        automator.can(BrowseTheWeb.with(browser));

        automator.attemptsTo(
                Open.url("http://practica.wantsome.ro/shop"));

        automator.should(
                seeThat(TheWebPage.title(), containsString("Wantsome Shop")));
    }

    @Test
    public void shouldBeAbleToSearchForProducts() {
        automator.can(BrowseTheWeb.with(browser));
        automator.attemptsTo(Open.url("http://practica.wantsome.ro/shop"));

        automator.attemptsTo(
                Click.on(".search-icon"),
                Enter.theValue("jeans").into(".search-field").
                        thenHit(Keys.ENTER)
                );

        automator.should(
                seeThat(TheTarget.textValuesOf(
                                Target.the("list of results").
                                        located(By.cssSelector(".entry-title>a"))),
                        contains("JEANS", "JEANS AND SHOE")
                )
        );
    }

    @Test
    public void shouldBeAbleToSearchForProductsRefactored() {
        automator.can(BrowseTheWeb.with(browser));
        automator.attemptsTo(Open.url("http://practica.wantsome.ro/shop"));

        automator.attemptsTo(Search.forProduct("jeans"));

        automator.should(seeThat(TheSearchResults.list(),
                        contains("JEANS", "JEANS AND SHOE")));
    }
}
